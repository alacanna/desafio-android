package com.amandalacanna.desafio.di.components

import android.app.Application
import com.amandalacanna.desafio.application.ConcreteApplication
import com.amandalacanna.desafio.di.modules.ActivityModule
import com.amandalacanna.desafio.di.modules.ApiModule
import com.amandalacanna.desafio.di.modules.AppModule
import com.amandalacanna.desafio.di.modules.FragmentModule
import com.amandalacanna.desafio.features.pullrequest.adapter.PullRequestAdapter
import com.amandalacanna.desafio.features.repository.adapter.RepositoryAdapter
import dagger.BindsInstance
import dagger.Component
import dagger.android.AndroidInjectionModule
import javax.inject.Singleton

@Singleton
@Component(modules = [(AndroidInjectionModule::class), (AppModule::class), (ActivityModule::class), (FragmentModule::class), (ApiModule::class)])
interface AppComponent {

    fun inject(application: ConcreteApplication)
    fun inject(adapter: RepositoryAdapter)
    fun inject(adapter: PullRequestAdapter)

    @Component.Builder
    interface Builder {
        @BindsInstance
        fun application(application: Application): Builder

        fun appModule(appModule: AppModule): Builder
        fun build(): AppComponent
    }
}