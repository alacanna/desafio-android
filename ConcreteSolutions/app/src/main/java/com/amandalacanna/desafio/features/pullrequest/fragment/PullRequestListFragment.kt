package com.amandalacanna.desafio.features.pullrequest.fragment

import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.app.AppCompatActivity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import com.amandalacanna.data.PullRequestData
import com.amandalacanna.desafio.R
import com.amandalacanna.desafio.di.Injectable
import com.amandalacanna.desafio.extensions.isVisible
import com.amandalacanna.desafio.features.pullrequest.adapter.PullRequestAdapter
import com.amandalacanna.desafio.features.viewmodel.RepositoryViewModel
import com.amandalacanna.desafio.router.RouterImpl
import io.reactivex.disposables.CompositeDisposable
import kotlinx.android.synthetic.main.pullrequest_list_fragment.*
import javax.inject.Inject

class PullRequestListFragment : Fragment(), Injectable {

    @Inject
    lateinit var viewModel: RepositoryViewModel

    @Inject
    lateinit var pullRequestAdapter: PullRequestAdapter

    @Inject
    lateinit var router: RouterImpl

    private val disposables by lazy { CompositeDisposable() }

    companion object {
        private val OWNER = "owner"
        private val REPOSITORY_NAME = "repositoryName"

        fun newInstance(owner: String, repositoryName: String): PullRequestListFragment {
            val args = Bundle()
            with(args) {
                putSerializable(OWNER, owner)
                putSerializable(REPOSITORY_NAME, repositoryName)
            }

            val fragment = PullRequestListFragment()
            fragment.arguments = args

            return fragment
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.pullrequest_list_fragment, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        initListView()

        arguments?.let {
            viewModel.inputs.getPullRequest(it.getString(OWNER), it.getString(REPOSITORY_NAME))
        }

        bindOutputs()
    }

    override fun onStop() {
        super.onStop()
        disposables.clear()
    }

    private fun bindOutputs() = with(viewModel.outputs) {
        val listRepositoryDataDisposable = listPullRequestData.subscribe {
            listPullRequestData ->
            if (listPullRequestData.isEmpty()) {
                showPlaceHolder()
            } else {
                pullRequestAdapter.setListPullRequest(listPullRequestData)
            }

            setOpenedCount(listPullRequestData)
            setClosedCount(listPullRequestData)
        }

        val isLoadingDisposable = isLoading.subscribe { showProgress ->
            progressBar.isVisible(showProgress)
        }

        val hasErrorDisposable = hasError.subscribe({
            messsage ->
            Toast.makeText(context, messsage, Toast.LENGTH_LONG).show()
        })

        val listClickListenerDisposable = pullRequestListener.subscribe {
            pullRequestData ->
            router.goToAfertListPullRequestClicked(activity as AppCompatActivity, pullRequestData)
        }

        disposables.addAll(listRepositoryDataDisposable,
                isLoadingDisposable,
                hasErrorDisposable,
                listClickListenerDisposable)
    }

    private fun showPlaceHolder() {
        list_pull_request.visibility = View.GONE
        txt_place_holder.visibility = View.VISIBLE
    }

    private fun setClosedCount(listPullRequestData: List<PullRequestData>) {
        val countClosed = listPullRequestData.filter { pullRequestData -> pullRequestData.state == "close" }.size
        val closed = " / $countClosed closed"
        txt_closed.text = closed
    }

    private fun setOpenedCount(listPullRequestData: List<PullRequestData>) {
        val countOpened = listPullRequestData.filter { pullRequestData -> pullRequestData.state == "open" }.size
        val opened = "$countOpened opened"
        txt_opened.text = opened
    }

    private fun initListView() {
        list_pull_request.layoutManager = list_pull_request.layoutManager
        list_pull_request.adapter = pullRequestAdapter
    }
}

