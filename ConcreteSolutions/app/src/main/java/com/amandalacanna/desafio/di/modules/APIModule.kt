package com.amandalacanna.desafio.di.modules

import com.amandalacanna.api.RestAPI
import com.amandalacanna.api.request.RepositoryRequest
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class ApiModule {
    @Provides
    @Singleton
    internal fun provideRestAPI(): RestAPI {
        return RestAPI()
    }

    @Provides
    @Singleton
    internal fun provideRepositoryRequest(restAPI: RestAPI): RepositoryRequest {
        return restAPI.repositoryRequest
    }

}