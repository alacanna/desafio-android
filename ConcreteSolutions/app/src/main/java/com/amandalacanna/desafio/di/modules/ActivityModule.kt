package com.amandalacanna.desafio.di.modules

import com.amandalacanna.desafio.features.main.MainActivity
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
internal abstract class ActivityModule {
    @ContributesAndroidInjector(modules = [(FragmentModule::class)])
    internal abstract fun contribuiteSplashActivity(): MainActivity
}