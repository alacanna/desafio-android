package com.amandalacanna.desafio.router

import android.support.v7.app.AppCompatActivity
import com.amandalacanna.data.RepositoryData
import com.amandalacanna.desafio.extensions.showFragment
import com.amandalacanna.desafio.features.pullrequest.fragment.PullRequestListFragment
import com.amandalacanna.desafio.features.repository.fragment.RepositoryListFragment
import javax.inject.Inject
import android.content.Intent
import android.net.Uri
import com.amandalacanna.data.PullRequestData

interface Router {
    fun startMainActivity(mActivity: AppCompatActivity)
    fun goToAfertListRepositoryClicked(mActivity: AppCompatActivity, repositoryData: RepositoryData)
    fun goToAfertListPullRequestClicked(mActivity: AppCompatActivity, pullRequestData: PullRequestData)

}

class RouterImpl @Inject constructor() : Router {
    override fun startMainActivity(mActivity: AppCompatActivity) {
        mActivity.showFragment(RepositoryListFragment.newInstance())
    }

    override fun goToAfertListRepositoryClicked(mActivity: AppCompatActivity, repositoryData: RepositoryData) {
        mActivity.showFragment(PullRequestListFragment.newInstance(repositoryData.owner.login, repositoryData.name))
    }

    override fun goToAfertListPullRequestClicked(mActivity: AppCompatActivity, pullRequestData: PullRequestData) {
        pullRequestData.html_url?.let {
            mActivity.startActivity(Intent(Intent.ACTION_VIEW, Uri.parse(pullRequestData.html_url)))
        }
    }
}