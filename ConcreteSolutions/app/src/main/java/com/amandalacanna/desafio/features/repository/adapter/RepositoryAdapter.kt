package com.amandalacanna.desafio.features.repository.adapter

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import com.amandalacanna.data.RepositoryData
import com.amandalacanna.desafio.databinding.RepositoryCellBinding
import com.amandalacanna.desafio.di.AppInjector
import com.amandalacanna.desafio.features.viewmodel.RepositoryViewModel
import java.util.ArrayList
import javax.inject.Inject

class RepositoryAdapter @Inject constructor(private val viewModel: RepositoryViewModel) : RecyclerView.Adapter<RepositoryViewHolder>() {
    var repositoryList: MutableList<RepositoryData> = ArrayList()

    init {
        AppInjector.dagger.inject(this)
    }

    fun setListRepository(listRepository: List<RepositoryData>) {
        this@RepositoryAdapter.repositoryList.addAll(listRepository)
        this@RepositoryAdapter.notifyItemInserted(itemCount - listRepository.size)
    }

    override fun getItemCount(): Int {
        return repositoryList.size
    }

    override fun onBindViewHolder(holder: RepositoryViewHolder?, position: Int) {
        holder?.setRepository(repositoryList[position])
        holder?.setImage(repositoryList[position].owner.avatar_url)
        holder?.clickListener(repositoryList[position])
     }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RepositoryViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val binding = RepositoryCellBinding.inflate(layoutInflater, parent, false)

        return RepositoryViewHolder(binding, viewModel)
    }
}
