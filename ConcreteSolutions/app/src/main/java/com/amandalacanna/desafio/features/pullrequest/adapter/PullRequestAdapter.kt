package com.amandalacanna.desafio.features.pullrequest.adapter

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import com.amandalacanna.data.PullRequestData
import com.amandalacanna.desafio.databinding.PullrequestCellBinding
import com.amandalacanna.desafio.di.AppInjector
import com.amandalacanna.desafio.features.viewmodel.RepositoryViewModel
import java.util.ArrayList
import javax.inject.Inject

class PullRequestAdapter @Inject constructor(private val viewModel: RepositoryViewModel) : RecyclerView.Adapter<PullRequestViewHolder>() {
    private var listPullrequest: MutableList<PullRequestData> = ArrayList()

    init {
        AppInjector.dagger.inject(this)
    }

    fun setListPullRequest(listPullReuquest: List<PullRequestData>) {
        this@PullRequestAdapter.listPullrequest.addAll(listPullReuquest)
        this@PullRequestAdapter.notifyItemInserted(itemCount - listPullReuquest.size)
    }

    override fun getItemCount(): Int {
        return listPullrequest.size
    }

    override fun onBindViewHolder(holder: PullRequestViewHolder?, position: Int) {
        holder?.setPullRequest(listPullrequest[position])
        holder?.setImage(listPullrequest[position].user.avatar_url)
        holder?.clickListener(listPullrequest[position])
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PullRequestViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val binding = PullrequestCellBinding.inflate(layoutInflater, parent, false)
        return PullRequestViewHolder(binding, viewModel)
    }
}
