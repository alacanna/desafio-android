package com.amandalacanna.desafio.features.repository.fragment

import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.app.AppCompatActivity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import com.amandalacanna.desafio.R
import com.amandalacanna.desafio.di.Injectable
import com.amandalacanna.desafio.extensions.isVisible
import com.amandalacanna.desafio.features.repository.adapter.RepositoryAdapter
import com.amandalacanna.desafio.features.viewmodel.RepositoryViewModel
import com.amandalacanna.desafio.router.RouterImpl
import com.amandalacanna.desafio.view.listener.EndlessScroll
import io.reactivex.disposables.CompositeDisposable
import kotlinx.android.synthetic.main.repository_list_fragment.*
import javax.inject.Inject

class RepositoryListFragment : Fragment(), Injectable {

    @Inject
    lateinit var viewModel: RepositoryViewModel

    @Inject
    lateinit var repositoryAdapter: RepositoryAdapter

    @Inject
    lateinit var router: RouterImpl

    private val disposables by lazy { CompositeDisposable() }

    private val txtPlaceHolder by lazy {
        txt_place_holder.setOnClickListener {
            txt_place_holder.visibility = View.GONE
            progressBar.visibility = View.VISIBLE
            viewModel.PAGE = 0
            viewModel.getRepository()
        }

        txt_place_holder
    }
    companion object {
        fun newInstance(): RepositoryListFragment {
            return RepositoryListFragment()
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.repository_list_fragment, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        if (repositoryAdapter.repositoryList.isEmpty()) {
            progressBar.visibility = View.VISIBLE
            viewModel.PAGE = 0
            viewModel.inputs.getRepository()
        }
    }

    override fun onStart() {
        super.onStart()
        bindOutputs()
        initListView()
    }
    override fun onStop() {
        super.onStop()
        disposables.clear()
    }

    private fun bindOutputs() = with(viewModel.outputs) {
        val listRepositoryDataDisposable = listRepositoryData.subscribe {
            repositoryAdapter.setListRepository(it)
        }

        val isLoadingDisposable = isLoading.subscribe { showProgress ->
           progressBar.isVisible(showProgress)
        }

        val hasErrorDisposable = hasError.subscribe({
            messsage ->
            if (viewModel.PAGE == 1) {
                txtPlaceHolder.visibility = View.VISIBLE
            }
            Toast.makeText(context, messsage, Toast.LENGTH_LONG).show()
        })

        val listClickListenerDisposable = repositoryListener.subscribe {
            repositoryData ->
                router.goToAfertListRepositoryClicked(activity as AppCompatActivity, repositoryData)
        }

        disposables.addAll(listRepositoryDataDisposable,
                isLoadingDisposable,
                hasErrorDisposable,
                listClickListenerDisposable)
    }

    private fun initListView() {
        list_repository.layoutManager = list_repository.layoutManager
        list_repository.adapter = repositoryAdapter
        list_repository.addOnScrollListener(EndlessScroll(viewModel))
    }
}

