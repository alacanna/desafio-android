package com.amandalacanna.desafio.features.repository.adapter

import android.support.v7.widget.RecyclerView
import com.amandalacanna.data.RepositoryData
import com.amandalacanna.desafio.databinding.RepositoryCellBinding
import com.amandalacanna.desafio.features.viewmodel.RepositoryViewModel
import com.squareup.picasso.Picasso

class RepositoryViewHolder (private val binding: RepositoryCellBinding, private val viewModel: RepositoryViewModel) : RecyclerView.ViewHolder(binding.root) {
    fun setRepository(repositoryData: RepositoryData) {
        binding.repositoryData = repositoryData
    }

    fun setImage (urlImage: String) {
        Picasso.with(binding.root.context)
                .load(urlImage)
                .into(binding.imgUser)
    }

    fun clickListener(repositoryData: RepositoryData) {
        binding.root.setOnClickListener { viewModel.clickRepository(repositoryData) }
    }
}