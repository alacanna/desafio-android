package com.amandalacanna.desafio.features.viewmodel

import com.amandalacanna.api.request.RepositoryRequest
import com.amandalacanna.data.PullRequestData
import com.amandalacanna.data.RepositoryData
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import io.reactivex.subjects.PublishSubject
import javax.inject.Inject
import javax.inject.Singleton

interface RepositoryViewModelInputs {
    fun getRepository()
    fun getPullRequest(owner: String, reposytoryName: String)
    fun <T>clickRepository(data: T)
}

interface RepositoryViewModelModelOutputs {
    val isLoading: Observable<Boolean>
    val listRepositoryData: Observable<List<RepositoryData>>
    val listPullRequestData: Observable<List<PullRequestData>>
    val hasError: Observable<String>
    val repositoryListener: Observable<RepositoryData>
    val pullRequestListener: Observable<PullRequestData>

}

interface RepositoryViewModelModelType {
    val inputs: RepositoryViewModelInputs
    val outputs: RepositoryViewModelModelOutputs
}
@Singleton
open class RepositoryViewModel @Inject constructor(private val request: RepositoryRequest) : RepositoryViewModelModelType, RepositoryViewModelInputs, RepositoryViewModelModelOutputs {
    var PAGE = 0

    // Initialize inputs and outputs
    override val inputs: RepositoryViewModelInputs
        get() = this

    override val outputs: RepositoryViewModelModelOutputs
        get() = this

    // Outputs implementations
    private val isLoadingSubject = PublishSubject.create<Boolean>()
    override val isLoading: Observable<Boolean>
        get() = isLoadingSubject

    private val listRepositorySubject = PublishSubject.create<List<RepositoryData>>()
    override val listRepositoryData: Observable<List<RepositoryData>>
        get() = listRepositorySubject

    private val listPullRequestSubject = PublishSubject.create<List<PullRequestData>>()
    override val listPullRequestData: Observable<List<PullRequestData>>
        get() = listPullRequestSubject

    private val hasErrorSubject = PublishSubject.create<String>()
    override val hasError: Observable<String>
        get() = hasErrorSubject

    private val repositoryListenerSubject = PublishSubject.create<RepositoryData>()
    override val repositoryListener: Observable<RepositoryData>
        get() = repositoryListenerSubject

    private val pullRequestListenerSubject = PublishSubject.create<PullRequestData>()
    override val pullRequestListener: Observable<PullRequestData>
        get() = pullRequestListenerSubject

    override fun getRepository() {
        request.getRepository(PAGE++)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe { isLoadingSubject.onNext(true) }
                .doOnError { isLoadingSubject.onNext(false) }
                .doOnComplete { isLoadingSubject.onNext(false) }
                .subscribe(
                        {
                            listRepositorySubject.onNext(it.items)
                        }, { _ ->
                            hasErrorSubject.onNext("Ocorreu um erro inesperado, tente novamente.")
                        })
    }

    override fun getPullRequest(owner: String, reposytoryName: String) {
        request.getPullRequest(owner, reposytoryName)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe { isLoadingSubject.onNext(true) }
                .doOnError { isLoadingSubject.onNext(false) }
                .doOnComplete { isLoadingSubject.onNext(false) }
                .subscribe(
                        {
                            listPullRequestSubject.onNext(it)
                        },
                        {
                            hasErrorSubject.onNext("Ocorreu um erro inesperado, tente novamente.")
                        })
    }

    override fun <T> clickRepository(data: T) {
        when (data) {
            is PullRequestData -> pullRequestListenerSubject.onNext(data)
            is RepositoryData -> repositoryListenerSubject.onNext(data)
        }
    }
}
