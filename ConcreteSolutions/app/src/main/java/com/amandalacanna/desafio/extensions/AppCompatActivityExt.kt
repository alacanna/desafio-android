package com.amandalacanna.desafio.extensions

import android.support.v4.app.Fragment
import android.support.v7.app.AppCompatActivity
import com.amandalacanna.desafio.R

fun AppCompatActivity.showFragment(fragment: Fragment) =
        supportFragmentManager.beginTransaction()
        .replace(R.id.frame_container, fragment)
        .addToBackStack(null)
        .commitAllowingStateLoss()
