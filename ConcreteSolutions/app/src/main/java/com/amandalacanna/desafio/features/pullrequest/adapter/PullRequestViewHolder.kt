package com.amandalacanna.desafio.features.pullrequest.adapter

import android.support.v7.widget.RecyclerView
import com.amandalacanna.data.PullRequestData
import com.amandalacanna.desafio.databinding.PullrequestCellBinding
import com.amandalacanna.desafio.features.viewmodel.RepositoryViewModel
import com.squareup.picasso.Picasso

class PullRequestViewHolder(private val binding: PullrequestCellBinding, private val viewModel: RepositoryViewModel) : RecyclerView.ViewHolder(binding.root) {
    fun setPullRequest(pullRequestData: PullRequestData) {
        binding.pullRequestData = pullRequestData
    }

    fun setImage (urlImage: String) {
        Picasso.with(binding.root.context)
                .load(urlImage)
                .into(binding.imgUser)
    }

    fun clickListener(pullRequestData: PullRequestData) {
        binding.root.setOnClickListener { viewModel.clickRepository(pullRequestData) }
    }
}