package com.amandalacanna.desafio.di.modules

import com.amandalacanna.desafio.features.pullrequest.fragment.PullRequestListFragment
import com.amandalacanna.desafio.features.repository.fragment.RepositoryListFragment
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
internal abstract class FragmentModule {
    @ContributesAndroidInjector
    internal abstract fun contribuiteRepositoryListFragment(): RepositoryListFragment

    @ContributesAndroidInjector
    internal abstract fun contribuitePullRequestListFragment(): PullRequestListFragment
}