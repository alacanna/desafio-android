package com.amandalacanna.desafio.features.viewmodel

import com.amandalacanna.api.request.RepositoryRequest
import com.amandalacanna.api.response.RepositoryResponse
import com.amandalacanna.data.PullRequestData
import io.reactivex.Observable
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mockito
import org.mockito.junit.MockitoJUnitRunner
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import io.reactivex.Scheduler
import io.reactivex.android.plugins.RxAndroidPlugins
import io.reactivex.internal.schedulers.ExecutorScheduler
import io.reactivex.plugins.RxJavaPlugins
import org.junit.Assert
import org.junit.Before
import org.mockito.InjectMocks
import org.mockito.Mock
import java.util.concurrent.Executor

@RunWith(MockitoJUnitRunner::class)
class RepositoryViewModelTest {
    val OWNER = "iluwatar"
    val REPOSITORY_NAME = "java-design-patterns"

    private val immediate = object : Scheduler() {
        override fun createWorker(): Scheduler.Worker {
            return ExecutorScheduler.ExecutorWorker(Executor { it.run() })
        }
    }
    private val mockRepositoryResult: String get() {
        return "{\n" +
                "  \"total_count\": 4189941,\n" +
                "  \"incomplete_results\": false,\n" +
                "  \"items\": [\n" +
                "    {\n" +
                "      \"id\": 7508411,\n" +
                "      \"name\": \"RxJava\",\n" +
                "      \"description\": \"RxJava – Reactive Extensions for the JVM – a library for composing asynchronous and event-based programs using observable sequences for the Java VM.\",\n" +
                "      \"forks_count\": 5373,\n" +
                "      \"stargazers_count\": 30567,\n" +
                "      \"owner\": {\n" +
                "        \"login\": \"ReactiveX\",\n" +
                "        \"avatar_url\": \"https://avatars1.githubusercontent.com/u/6407041?v=4\"\n" +
                "      }\n" +
                "    },\n" +
                "    {\n" +
                "      \"id\": 22790488,\n" +
                "      \"name\": \"java-design-patterns\",\n" +
                "      \"description\": \"Design patterns implemented in Java\",\n" +
                "      \"owner\": {\n" +
                "        \"login\": \"iluwatar\",\n" +
                "        \"avatar_url\": \"https://avatars1.githubusercontent.com/u/582346?v=4\"\n" +
                "      },\n" +
                "      \"stargazers_count\": 29173,\n" +
                "      \"forks_count\": 9407\n" +
                "    },\n" +
                "    {\n" +
                "      \"id\": 507775,\n" +
                "      \"name\": \"elasticsearch\",\n" +
                "      \"description\": \"Open Source, Distributed, RESTful Search Engine\",\n" +
                "      \"owner\": {\n" +
                "        \"login\": \"elastic\",\n" +
                "        \"avatar_url\": \"https://avatars0.githubusercontent.com/u/6764390?v=4\"\n" +
                "      },\n" +
                "      \"stargazers_count\": 28421,\n" +
                "      \"forks_count\": 9903\n" +
                "    }\n" +
                "  ]\n" +
                "}"
    }
    private val mockPullRequestResult: String get() {
        return "[\n" +
                "  {\n" +
                "    \"state\": \"open\",\n" +
                "    \"title\": \"Fix typo\",\n" +
                "    \"user\": {\n" +
                "      \"login\": \"ryanguest\",\n" +
                "      \"avatar_url\": \"https://avatars0.githubusercontent.com/u/488119?v=4\"\n" +
                "    },\n" +
                "    \"body\": \"Fix typo\\r\\nThis fixes a minor type in a code comment.\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n> For detailed contributing instructions seehttps://github.com/iluwatar/java-design-patterns/wiki/01.-How-to-contribute\\r\\n\"\n" +
                "  },\n" +
                "  {\n" +
                "    \"state\": \"open\",\n" +
                "    \"title\": \"#677 add pattern Trampoline.\",\n" +
                "    \"user\": {\n" +
                "      \"login\": \"besok\",\n" +
                "      \"avatar_url\": \"https://avatars2.githubusercontent.com/u/29834592?v=4\"\n" +
                "    },\n" +
                "    \"body\": \"Pull request title\\r\\n\\r\\n- Add pattern trampoline.\\r\\n- #677 -> https://github.com/iluwatar/java-design-patterns/issues/677\\r\\n\\r\\n\\r\\nPull request description\\r\\n\\r\\n- Add readme, code,test.\\r\\n\\r\\n\\r\\n\\r\\n> For detailed contributing instructions see https://github.com/iluwatar/java-design-patterns/wiki/01.-How-to-contribute\\r\\n\\r\\n\"\n" +
                "  },\n" +
                "  {\n" +
                "    \"state\": \"open\",\n" +
                "    \"title\": \"handleRequest method in RequestHandler can introduce 'template method'\",\n" +
                "    \"user\": {\n" +
                "      \"login\": \"maxtmn\",\n" +
                "      \"avatar_url\": \"https://avatars0.githubusercontent.com/u/4903278?v=4\"\n" +
                "    },\n" +
                "    \"body\": \"with hook method. Also Request.isHandeld() can be used to interrupt\\r\\nchain\\r\\n\\r\\n\\r\\nPull request title\\r\\n\\r\\n- Clearly and concisely describes what it does\\r\\n- Refer to the issue that it solves, if available\\r\\n\\r\\n\\r\\nPull request description\\r\\n\\r\\n- Describes the main changes that come with the pull request\\r\\n- Any relevant additional information is provided\\r\\n\\r\\n\\r\\n\\r\\n> For detailed contributing instructions see https://github.com/iluwatar/java-design-patterns/wiki/01.-How-to-contribute\\r\\n\"\n" +
                "  }\n" +
                "]"
    }

    @Before
    fun setUp() {
        setUpRxThreadControl()
    }

    @Mock
    lateinit var repositoryRequest: RepositoryRequest

    @InjectMocks
    lateinit var viewModel: RepositoryViewModel

    var repositoryMock = Gson().fromJson<RepositoryResponse>(mockRepositoryResult, RepositoryResponse::class.java)

    var listType = object : TypeToken<List<PullRequestData>>() {}.type
    var pullRequestMock = Gson().fromJson<List<PullRequestData>>(mockPullRequestResult, listType)

    /* Tests when Repository is called */
    @Test
    fun shouldStartShowingProgressAndFinishHideItRepository() {
        Mockito.`when`(repositoryRequest.getRepository(viewModel.PAGE)).thenReturn(Observable.just(repositoryMock))

        val subscriber = viewModel.outputs.isLoading.test()
        viewModel.inputs.getRepository()

        subscriber.assertValues(true, false)
    }

    @Test
    fun shouldShowRequestErrorMessageWhenFetchDataFailsRepository() {
        Mockito.`when`(repositoryRequest.getRepository(viewModel.PAGE))
                .thenReturn(Observable.unsafeCreate<RepositoryResponse> { subscriber -> subscriber.onError(RuntimeException()) })

        val errorSubscriber = viewModel.outputs.hasError.test()
        viewModel.inputs.getRepository()

        errorSubscriber.assertValue { "Ocorreu um erro inesperado, tente novamente." == it }
    }

    @Test
    fun shouldInitRepositoryList() {
        Mockito.`when`(repositoryRequest.getRepository(viewModel.PAGE)).thenReturn(Observable.just(repositoryMock))

        val listSubscriber = viewModel.outputs.listRepositoryData.test()
        viewModel.inputs.getRepository()

        listSubscriber.assertOf {
            val listRepository = listSubscriber.values()[0]
            Assert.assertEquals(listRepository, repositoryMock.items)
        }
    }

    /* Tests when PullRequest is called */
    @Test
    fun shouldStartShowingProgressAndFinishHideItPullRequest() {
        Mockito.`when`(repositoryRequest.getPullRequest(OWNER, REPOSITORY_NAME))
                .thenReturn(Observable.just(pullRequestMock))

        val subscriber = viewModel.outputs.isLoading.test()
        viewModel.inputs.getPullRequest(OWNER, REPOSITORY_NAME)

        subscriber.assertValues(true, false)
    }

    @Test
    fun shouldShowRequestErrorMessageWhenFetchDataFailsPullRequest() {
        Mockito.`when`(repositoryRequest.getPullRequest(OWNER, REPOSITORY_NAME))
                .thenReturn(Observable.unsafeCreate<List<PullRequestData>> { subscriber -> subscriber.onError(RuntimeException()) })

        val errorSubscriber = viewModel.outputs.hasError.test()
        viewModel.inputs.getPullRequest(OWNER, REPOSITORY_NAME)

        errorSubscriber.assertValue { "Ocorreu um erro inesperado, tente novamente." == it }
    }

    @Test
    fun shouldInitPullRequestList() {
        Mockito.`when`(repositoryRequest.getPullRequest(OWNER, REPOSITORY_NAME))
                .thenReturn(Observable.just(pullRequestMock))

        val listSubscriber = viewModel.outputs.listPullRequestData.test()
        viewModel.inputs.getPullRequest(OWNER, REPOSITORY_NAME)

        listSubscriber.assertOf {
            val listPurequest = listSubscriber.values()[0]
            Assert.assertEquals(listPurequest, pullRequestMock)
        }
    }

    private fun setUpRxThreadControl() {
        RxJavaPlugins.setInitIoSchedulerHandler { immediate }
        RxJavaPlugins.setInitComputationSchedulerHandler { immediate }
        RxJavaPlugins.setNewThreadSchedulerHandler { immediate }
        RxJavaPlugins.setInitSingleSchedulerHandler { immediate }
        RxAndroidPlugins.setInitMainThreadSchedulerHandler { immediate }
    }
}