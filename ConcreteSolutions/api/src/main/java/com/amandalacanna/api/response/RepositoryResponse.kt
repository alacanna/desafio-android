package com.amandalacanna.api.response

import com.amandalacanna.data.RepositoryData

data class RepositoryResponse(val total_count: Long,
                              val incomplete_results: Boolean,
                              val items: List<RepositoryData>)