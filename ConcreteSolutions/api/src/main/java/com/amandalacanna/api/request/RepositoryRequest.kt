package com.amandalacanna.api.request

import com.amandalacanna.api.response.RepositoryResponse
import com.amandalacanna.data.PullRequestData
import io.reactivex.Observable
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface RepositoryRequest {
    @GET("search/repositories?q=language:Java&sort=stars")
    fun getRepository(@Query("page") page: Int): Observable<RepositoryResponse>

    @GET("repos/{owner}/{repositoryName}/pulls")
    fun getPullRequest(@Path("owner") owner: String, @Path("repositoryName") repositoryName: String): Observable<List<PullRequestData>>
}