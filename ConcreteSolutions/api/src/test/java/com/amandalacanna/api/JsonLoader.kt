package com.amandalacanna.api

import java.io.IOException
import com.google.common.io.ByteStreams

class JsonLoader {
    @Throws(IOException::class)
    fun loadJson(filename: String): String {
        val inputStream = this.javaClass.classLoader.getResourceAsStream(filename)
        return String(ByteStreams.toByteArray(inputStream))
    }
}