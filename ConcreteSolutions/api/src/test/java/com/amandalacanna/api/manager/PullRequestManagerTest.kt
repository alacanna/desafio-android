package com.amandalacanna.api.manager

import com.amandalacanna.api.Constants
import com.amandalacanna.api.JsonLoader
import com.amandalacanna.api.RestAPI
import com.amandalacanna.api.request.RepositoryRequest
import com.amandalacanna.data.PullRequestData
import junit.framework.Assert
import okhttp3.mockwebserver.MockResponse
import okhttp3.mockwebserver.MockWebServer
import org.junit.Test
import org.junit.Before
import junit.framework.Assert.assertEquals

class PullRequestManagerTest {
    val OWNER = "iluwatar"
    val REPOSITORY_NAME = "java-design-patterns"

    private var server: MockWebServer? = null
    private var repositoryRequest: RepositoryRequest? = null

    @Before
    @Throws(Exception::class)
    fun setup() {
        server = MockWebServer()
        val baseUrl = server?.url("/")
        repositoryRequest = RestAPI(baseUrl).repositoryRequest
    }

    @Test
    @Throws(Exception::class)
    fun testAllSetAsNotNull() {
        Assert.assertNotNull(server)
        Assert.assertNotNull(repositoryRequest)
    }

    @Test
    @Throws(Exception::class)
    fun testRequestWithSuccess() {
        val mockResponse = MockResponse()
        mockResponse.setResponseCode(Constants.ResponseCode.SUCCESS)
        mockResponse.setBody(JsonLoader().loadJson(Constants.ResponseFile.PULL_REQUEST))
        server?.enqueue(mockResponse)

        val pullRequestData = repositoryRequest?.getPullRequest(OWNER, REPOSITORY_NAME)?.blockingFirst()
        matchPullRequestInfo(pullRequestData)
    }

    private fun matchPullRequestInfo(pullRequestData: List<PullRequestData>?) {
        pullRequestData?.forEachIndexed {
            index, pullRequestData ->
            when (index) {
                0 -> {
                    assertEquals("open", pullRequestData.state)
                    assertEquals("Fix typo", pullRequestData.title)
                    assertEquals("Fix typo\r\nThis fixes a minor type in a code comment.\r\n\r\n\r\n\r\n\r\n\r\n\r\n> " +
                            "For detailed contributing instructions seehttps://github.com/iluwatar/java-design-patterns/wiki/01.-How-to-contribute\r\n"
                            , pullRequestData.body)
                    assertEquals("ryanguest", pullRequestData.user.login)
                    assertEquals("https://avatars0.githubusercontent.com/u/488119?v=4", pullRequestData.user.avatar_url)
                }
                1 -> {
                    assertEquals("open", pullRequestData.state)
                    assertEquals("#677 add pattern Trampoline.", pullRequestData.title)
                    assertEquals("Pull request title\r\n\r\n- Add pattern trampoline.\r\n- #677 -> " +
                            "https://github.com/iluwatar/java-design-patterns/issues/677\r\n\r\n\r\nPull request " +
                            "description\r\n\r\n- Add readme, code,test.\r\n\r\n\r\n\r\n> For detailed contributing " +
                            "instructions see https://github.com/iluwatar/java-design-patterns/wiki/01.-How-to-contribute\r\n\r\n",
                            pullRequestData.body)
                    assertEquals("besok", pullRequestData.user.login)
                    assertEquals("https://avatars2.githubusercontent.com/u/29834592?v=4", pullRequestData.user.avatar_url)

                }
                2 -> {
                    assertEquals("open", pullRequestData.state)
                    assertEquals("handleRequest method in RequestHandler can introduce 'template method'", pullRequestData.title)
                    assertEquals("with hook method. Also Request.isHandeld() can be used to interrupt\r\nchain\r\n\r\n\r\nPull request title" +
                            "\r\n\r\n- Clearly and concisely describes what it does\r\n- Refer to the issue that it solves, if available\r\n\r\n\r\n" +
                            "Pull request description\r\n\r\n- Describes the main changes that come with the pull request\r\n- " +
                            "Any relevant additional information is provided\r\n\r\n\r\n\r\n> For detailed contributing instructions see " +
                            "https://github.com/iluwatar/java-design-patterns/wiki/01.-How-to-contribute\r\n",
                            pullRequestData.body)
                    assertEquals("maxtmn", pullRequestData.user.login)
                    assertEquals("https://avatars0.githubusercontent.com/u/4903278?v=4", pullRequestData.user.avatar_url)
                }
            }
        }
    }

}