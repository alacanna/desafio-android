package com.amandalacanna.api.manager

import android.os.Build
import com.amandalacanna.api.Constants
import com.amandalacanna.api.JsonLoader
import com.amandalacanna.api.RestAPI
import com.amandalacanna.api.request.RepositoryRequest
import com.amandalacanna.api.response.RepositoryResponse
import junit.framework.Assert
import okhttp3.mockwebserver.MockResponse
import okhttp3.mockwebserver.MockWebServer
import org.junit.Test
import org.junit.Before
import junit.framework.Assert.assertEquals

class RepositoryManagerTest {

    private var server: MockWebServer? = null
    private var repositoryRequest: RepositoryRequest? = null

    @Before
    @Throws(Exception::class)
    fun setup() {
        server = MockWebServer()
        val baseUrl = server?.url("/")
        repositoryRequest = RestAPI(baseUrl).repositoryRequest
    }

    @Test
    @Throws(Exception::class)
    fun testAllSetAsNotNull() {
        Assert.assertNotNull(server)
        Assert.assertNotNull(repositoryRequest)
    }

    @Test
    @Throws(Exception::class)
    fun testRequestWithSuccess() {
        val mockResponse = MockResponse()
        mockResponse.setResponseCode(Constants.ResponseCode.SUCCESS)
        mockResponse.setBody(JsonLoader().loadJson(Constants.ResponseFile.REPOSITORY))
        server?.enqueue(mockResponse)

        val repositoryResponse = repositoryRequest?.getRepository(1)?.blockingFirst()
        assertRepositoryResults(repositoryResponse)
        matchRepositoryInfo(repositoryResponse)
    }

    private fun assertRepositoryResults(repositoryResponse: RepositoryResponse?) {
        assertEquals(4189941.toLong(), repositoryResponse?.total_count)
        assertEquals(false, repositoryResponse?.incomplete_results)
        assertEquals(3, repositoryResponse?.items?.size)
    }

    private fun matchRepositoryInfo(repositoryResponse: RepositoryResponse?) {
        repositoryResponse?.items?.forEachIndexed {
            index, repositoryData ->
            when (index) {
                0 -> {
                    assertEquals(7508411.toLong(), repositoryData.id)
                    assertEquals("RxJava", repositoryData.name)
                    assertEquals("RxJava – Reactive Extensions for the JVM – a library for " +
                            "composing asynchronous and event-based programs using " +
                            "observable sequences for the Java VM.", repositoryData.description)
                    assertEquals(5373, repositoryData.forks_count)
                    assertEquals(30567, repositoryData.stargazers_count)
                    assertEquals("ReactiveX", repositoryData.owner.login)
                    assertEquals("https://avatars1.githubusercontent.com/u/6407041?v=4",
                            repositoryData.owner.avatar_url)
                }
                1 -> {
                    assertEquals(22790488.toLong(), repositoryData.id)
                    assertEquals("java-design-patterns", repositoryData.name)
                    assertEquals("Design patterns implemented in Java", repositoryData.description)
                    assertEquals(9407, repositoryData.forks_count)
                    assertEquals(29173, repositoryData.stargazers_count)
                    assertEquals("iluwatar", repositoryData.owner.login)
                    assertEquals("https://avatars1.githubusercontent.com/u/582346?v=4",
                            repositoryData.owner.avatar_url)
                }
                2 -> {
                    assertEquals(507775.toLong(), repositoryData.id)
                    assertEquals("elasticsearch", repositoryData.name)
                    assertEquals("Open Source, Distributed, RESTful Search Engine", repositoryData.description)
                    assertEquals(9903, repositoryData.forks_count)
                    assertEquals(28421, repositoryData.stargazers_count)
                    assertEquals("elastic", repositoryData.owner.login)
                    assertEquals("https://avatars0.githubusercontent.com/u/6764390?v=4",
                            repositoryData.owner.avatar_url)
                }
            }
        }
    }

}