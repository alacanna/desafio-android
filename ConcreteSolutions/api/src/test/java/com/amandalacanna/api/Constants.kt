package com.amandalacanna.api

class Constants {
    object ResponseCode {
        val SUCCESS = 200
    }

    object ResponseFile {
        val REPOSITORY = "repository.json"
        val PULL_REQUEST = "pullRequest.json"

    }
}

