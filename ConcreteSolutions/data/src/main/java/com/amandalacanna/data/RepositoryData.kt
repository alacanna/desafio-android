package com.amandalacanna.data

data class RepositoryData(val id: Long,
                          val name: String,
                          val description: String,
                          val forks_count: Int,
                          val stargazers_count: Int,
                          val owner: Owner)