package com.amandalacanna.data

data class PullRequestData(val title: String,
                           val body: String,
                           val state: String,
                           val user: Owner,
                           val html_url: String?)