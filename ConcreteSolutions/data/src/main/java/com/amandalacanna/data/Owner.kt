package com.amandalacanna.data

data class Owner(val login: String,
                 val avatar_url: String,
                 val name: String?)